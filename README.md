# [mn]medianet - Price module - Overview
The Pricing Module is designed for storing, managing and evaluating product prices.
The Shop owner buy the products from suppliers/manufactures, sometimes he will buy products with taxes and sometimes without tax.
The shop owner buy products in any curency (EUR, DOLLER, TL.. etc) and he will also get the discount in the same currency and to deliver/get those products to warehouse he may pay shipping cost or any other additional surcharge.
By considering the above concept, 
The Pricing Module is designed with following fields:

## Supplier price fields
1. EK Netto / Purchase price without tax
2. Währung / Currency (Currency exchange rate can be managed in currency management)
3. Rabatt / Discount (Will be in %)
4. Aufschlag / Surcharge (will be in shop default currency)
5. EK Brutto Purchase price with taxes

All Brutto / Including tax prices will be calculated from global tax value, This can be managed in shop configuration.

## Shop price fields
To sell these products in the shop, he will check his margin and apply it on the EK Brutto / Purchase price with taxes and calculate the Listenpreis Brutto and Listenpreis Netto.So additionally we have 3 more fields.
6. Marge / margin
7. Listenpreis Netto
8. Listenpreis Brutto


The EK calculation will be done in any manner either it can be done from EK Netto to EK Brutto.
To do reverse calculation, we need an additional option **"Recalculate"**
This Recalculation will subtract taxes from EK Brutto and calculates EK Netto and other supplier fields will be empty/zeros.

## Supplier calculation
***If EK Netto has value and "Recalculate" checkbox is not checked, EK Brutto calculation***
```
EK_Netto_Discount = EK_Netto - (EK_Netto * (Rabatt / 100));
EK_Netto_Exchange = EK_Netto_Discount / Exchange_Rate;
EK_Netto_With_Surcharge = EK_Netto_Exchange + Surcharge;
EK_Brutto = EK_Netto_With_Surcharge + (EK_Netto_With_Surcharge * (Global_Tax / 100));
```

***If EK Brutto has value and "Recalculate" checkbox is checked, EK Netto calculation***
```
EK_Netto = EK_Brutto -  ((EK_Brutto * Global_Tax) / (Global_Tax + 100));
Rabatt = 0;
Curency = Shop Default Currency;
Surcharge = 0;
```

## Shop price calculation
The Listenpreis will be calculated from EK Brutto if available, Otherwise we can also directly enter Listenpreis Brutto or Listenpreis_Netto.One will be calculated from other.
If we have EK Brutto, margin filled in, the calculation of Listenpreis Brutto and Listenpreis Netto as follows.
```
Listenpreis_Brutto = EK_Brutto +  (EK_Brutto * (Margin / 100));
Listenpreis_Netto = Listenpreis_Brutto -  ((Listenpreis_Brutto * Global_Tax) / (Global_Tax + 100));
```

In additional to the above price fields, we have some more fields, They are:
- MOQ (Default value = 1)
- Surcharge
- MOV
- Preis für Anzahl / Price per quantity (Default value = 1)
- Verpackungseinheit / Packaging unit (Default value = 1)

## Screenshots
**Single product Netto to Brutto**
![Alt text](/screenshots/Single_product_update-2.png?raw=true "Single Product Update  Netto - Brutto")

**Single product Brutto to Netto**
![Alt text](/screenshots/Single_product_update-1.png?raw=true "Single Product Update Brutto - Netto")

**Multiple products update**
![Alt text](/screenshots/Multiple_product_update.png?raw=true "Single Product Update")

**Currency management**
![Alt text](/screenshots/Currency_management.png?raw=true "Currency management")

**Shop configuration**
![Alt text](/screenshots/Shop_configuration.png?raw=true "Shop configuration")

#License
Copyright (c) [mn]medianet. All rights reserved.